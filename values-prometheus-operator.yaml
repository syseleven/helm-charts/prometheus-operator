---
# exporters
kubeDns:
  enabled: false
kubeControllerManager:
  enabled: false
kubeScheduler:
  enabled: false
kubeEtcd:
  enabled: false
kubeProxy:
  enabled: true
kubelet:
  enabled: true
nodeExporter:
  enabled: true

prometheus-node-exporter:
  resources:
    limits:
      cpu: 200m
      memory: 50Mi
    requests:
      cpu: 50m
      memory: 50Mi

# default rules
defaultRules:
  rules:
    etcd: false
    kubeScheduler: false

# prometheus operator
prometheusOperator:
  # Create CRDs separately in helmfile presync hooks
  # https://github.com/roboll/helmfile/issues/1124
  # https://github.com/helm/helm/issues/7449
  # https://github.com/cloudposse/helmfiles/blob/59490fd2599d6113a14103be919985f9fbcea73a/releases/prometheus-operator.yaml
  createCustomResource: false
  resources:
    limits:
      cpu: 100m
      memory: 200Mi
    requests:
      cpu: 100m
      memory: 200Mi

# alertmanager
alertmanager:
  enabled: true
  podAntiAffinity: hard
  alertmanagerSpec:
    replicas: 2
    ressources:
      requests:
        memory: 50Mi
        cpu: 100m
      limits:
        memory: 50Mi
        cpu: 100m
    storage:
      volumeClaimTemplate:
        spec:
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 1Gi
  config:
    # If you need to add more receivers, you need to add the null-reciver too
    # helm merge does not merge list-values.
    # https://github.com/helm/helm/issues/3486
    receivers:
      - name: "null"
    route:
      # default to null-receiver, override this in your cluster if needed
      receiver: "null"
      routes:
        - receiver: "null"
          match_re:
            # KubeAPILatencyHigh - too flappy
            # CPUThrottlingHigh - too flappy
            # Aggregated* see https://github.com/kubernetes/kube-aggregator/issues/23
            #   this waits for https://github.com/helm/charts/issues/22278 please check for new releases
            alertname: Watchdog|KubeAPILatencyHigh|CPUThrottlingHigh|AggregatedAPIErrors|AggregatedAPIDown

# prometheus
prometheus:
  prometheusSpec:
    replicas: 2
    podAntiAffinity: hard
    resources:
      requests:
        memory: 4Gi
        cpu: 2000m
      limits:
        memory: 4Gi
        cpu: 2000m
    retention: 90d
    storageSpec:
      volumeClaimTemplate:
        spec:
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 200Gi
    ruleSelector: {}
    ruleSelectorNilUsesHelmValues: false
    ruleNamespaceSelector: {}
    serviceMonitorSelector: {}
    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorNamespaceSelector: {}
    podMonitorSelector: {}
    podMonitorSelectorNilUsesHelmValues: false
    podMonitorNamespaceSelector: {}

# grafana
grafana:
  replicas: 2
  ingress:
    annotations:
      nginx.ingress.kubernetes.io/affinity: cookie
  persistence:
    enabled: false
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchLabels:
              app.kubernetes.io/name: grafana
          topologyKey: "kubernetes.io/hostname"
  adminPassword: ""
  deploymentStrategy:
    type: Recreate
  plugins:
    - grafana-piechart-panel
  sidecar:
    dashboards:
      watchMethod: SLEEP
      searchNamespace: ALL
    datasources:
      watchMethod: SLEEP
      searchNamespace: ALL
  resources:
    requests:
      memory: 200Mi
      cpu: 200m
    limits:
      memory: 200Mi
      cpu: 200m
