FROM syseleven/kubectl-helm:helm-3.2.0

WORKDIR /usr/local/share/helmfile/
COPY . .

VOLUME ["/root/.kube"]
ENTRYPOINT ["/usr/bin/helmfile"]
CMD ["apply"]
