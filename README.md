# prometheus-operator helmfile bundle

This bundle deploys prometheus-operator with best a basic practice configuration and additional things like dashboards.

## Table of Contents

1. [Deployment](#deployment)
    1. [Via Gitlab CI](#via-gitlab-ci)
    1. [Manual](#manual)
1. [Manual tasks](#manual-tasks)
    1. [Forward alertmanager to localhost](#forward-alertmanager-to-localhost)
    1. [Send test alerts](#send-test-alerts)
1. [Known issues](#known-issues)
    1. [Configuring alertmanager receivers](#configuring-alertmanager-receivers)
1. [Release](#release)
1. [Usage disclaimer](#usage-disclaimer)

## Deployment

### Via Gitlab CI

The following examples expects you to have the following prerequisites:

* You have a Gitlab Project
* You have two `KUBECONFIG` Gitlab CI variables defined which are masked (base64 encoded) and tagged with the matching environment: `production`/`stage`

Create the following directory/file structure in your (control) repository:
```bash
└── syseleven-managed-prometheus-operator
    ├── .gitlab-ci.yml
    ├── values-prometheus-operator-production.yaml
    ├── values-prometheus-operator-stage.yaml
    └── values-prometheus-operator.yaml
```

prometheus-operator/.gitlab-ci.yml:
```yaml
include:
  - project: syseleven/helm-charts/prometheus-operator
    file: PrometheusOperator.yaml
    ref: master

managed-services-prometheus-operator-diff-production:
  extends: .managed-services-prometheus-operator-diff
  environment:
    name: production

managed-services-prometheus-operator-deploy-production:
  extends: .managed-services-prometheus-operator-deploy
  variables:
    CUSTOM_COMMAND: curl -d 'deployment done' https://rocketchat.syseleven.de
  environment:
    name: production

managed-services-prometheus-operator-diff-stage:
  extends: .managed-services-prometheus-operator-diff
  environment:
    name: stage

managed-services-prometheus-operator-deploy-stage:
  extends: .managed-services-prometheus-operator-deploy
  environment:
    name: stage
```

Fill in customer/environment specific configuration in prometheus-operator/values-prometheus-operator.yaml and prometheus-operator/values-prometheus-operator-${ENVIRONMENT}.yaml.

prometheus-operator/values-prometheus-operator.yaml:
```yaml
prometheus:
  prometheusSpec:
    image:
      repository: registry.example.com/custom/prometheus
      tag: v2.17.1
```

prometheus-operator/values-prometheus-operator-stage.yaml:
```yaml
grafana:
  adminPassword: my-stage-password
```

prometheus-operator/values-prometheus-operator-production.yaml:
```yaml
grafana:
  adminPassword: highly-secure-production-password
```

### Manual

You can also manually deploy the directory structure mentioned above. This might be useful for bootstraping, debugging or development.

### docker

```bash
cd syseleven-managed-prometheus-operator

# Diff
docker run -v $PATH_TO_KUBECONFIG:/root/.kube/config -v $PWD:/src -e BASEDIR=/src registry.gitlab.com/syseleven/helm-charts/prometheus-operator:${TAG:-latest} diff

# Apply
docker run -v $PATH_TO_KUBECONFIG:/root/.kube/config -v $PWD:/src -e BASEDIR=/src registry.gitlab.com/syseleven/helm-charts/prometheus-operator:${TAG:-latest} apply
```

### helmfile

You can use helmfile locally to apply the helmfile bundle.

```bash
$ cd syseleven-managed-prometheus-operator
$ cat helmfile.yaml
helmfiles:
  # Prefer to use version in ref
  - path: git::https://gitlab.com/syseleven/helm-charts/prometheus-operator@helmfile.yaml?ref=master

# Diff
$ helmfile diff

# Apply
$ helmfile apply
```

## Manual tasks

### Forward alertmanager to localhost

```sh
kubectl port-forward -n syseleven-managed-prometheus-operator alertmanager-prometheus-operator-alertmanager-0 9093
```

### Send test alerts

You can send test alerts to a alertmanager instance to test alerting.

```sh
kubectl port-forward -n syseleven-managed-prometheus-operator alertmanager-prometheus-operator-alertmanager-0 9093 &

curl -H "Content-Type: application/json" -d '[{"labels":{"alertname":"myalert"}}]' localhost:9093/api/v1/alerts
```

## Known issues

### Configuring alertmanager receivers

When adding an additional receiver, you need to copy the null receiver config into your own cluster configuration as well. Helm does not merge list values and you won't have the needed null-receiver configured.

```yaml
alertmanager:
  config:
    receivers:
    - name: "null" # Add this to your config as well
    - name: myotherreceiver
      webhook_configs:
      - send_resolved: true
        url: https://myurl
```

## Release

* Bump variables.VERSION in PrometheusOperator.yaml
* Tag git release
* Push git tag

## Usage disclaimer

The use of this bundle on non SysEleven infrastructure (e.g. MetaKube) is prohibited.
